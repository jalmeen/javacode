package com;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Calculator {

    private static final Logger LOG = LogManager.getLogger(Calculator.class);

    int num1,num2;

    public Calculator(int num1,int num2){
        this.num1 = num1;
        this.num2 = num2;

        LOG.info("Calculator Initialized num1 = {} num2 = {} sum = {}",num1,num2);
    }

    public int add(){
        LOG.info("Add method Called");
        return num1+num2;
    }

    public int subtract(){
        LOG.info("Subtract method Called");
        return num1-num2;
    }

    public int divide(){
        LOG.info("Divide method Called");

        int result = 0;

        if(num2 == 0){
            try {
                result = num1/num2;
            }
            catch (Exception e){
                LOG.fatal("Divide by zero = " + e.toString());
            }

        }
        return result;
    }

    public int multiply(){
        LOG.info("Multiply method Called");
        return num1*num2;
    }
}